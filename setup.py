from setuptools import setup

setup(
   name='krigingOptimizer',
   version='0.1',
   description='A useful module',
   author='Yegor',
   packages=['krigingOptimizer'],  #same as name
   install_requires=['numpy', 'keras', 'pyKriging'], #external packages as dependencies
)