'''
    ENG
    File is calculating cool points (on the hyperparameter plot)
    for neural network, where better to do calculations.
    
    Need to remake in file:
    1.To make function for denormolizing of hyperparameters
    2.To make a logging in file
    3.To make code for saving of calculations.
    4.To make code for loading of calculations
    5.To make SCRIPTS from file.
    
    RUS
    Файл для вычисления точек (в пространстве гиперпараметров),
    для автоматического подбора лучшей комбинации гиперпараметров нейронной
    сети.
    
    Что ещё необходимо реализовать:
    1.Создать функцию для денормализации значений гиперпараметров.
    2.Сделать логирование.
    3.Написать код для сохранения результатов вычислений. +
    4.Написать код для загрузки результатов
    5.Раскидать содержимое файла по отедльным скриптам.
    
'''


# Libraries for Kriging
from pyKriging.krige import kriging
from pyKriging.samplingplan import samplingplan
import numpy as np

# Libraries for neural networks
np.random.seed()
import keras
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.optimizers import RMSprop

# CONSTANTS
TOL = 0.0001
EPOCHES_MAX_VAL = 15
EPOCHES_MIN_VAL = 5
NEURONS_MAX_VAL = 1024
NEURONS_MIN_VAL = 128

SAVE_COUNTER = 0

class classOptimizer:

    def __init__(self):
        self.list = []

    def _create_model(self, norm_epochs, norm_neurons):

        epochs = norm_epochs*EPOCHES_MAX_VAL+(1-norm_neurons)*EPOCHES_MIN_VAL
        neurons = norm_epochs*EPOCHES_MAX_VAL+(1-norm_neurons)*EPOCHES_MIN_VAL

        batch_size = 128
        num_classes = 10

        (x_train, y_train), (x_test, y_test) = mnist.load_data()

        x_train = x_train.reshape(60000, 784)
        x_test = x_test.reshape(10000, 784)
        x_train = x_train.astype('float32')
        x_test = x_test.astype('float32')
        x_train /= 255
        x_test /= 255

        # convert class vectors to binary class matrices
        y_train = keras.utils.to_categorical(y_train, num_classes)
        y_test = keras.utils.to_categorical(y_test, num_classes)

        model = Sequential()
        model.add(Dense(neurons, activation='relu', input_shape=(784,)))
        model.add(Dropout(0.2))
        model.add(Dense(neurons, activation='relu'))
        model.add(Dropout(0.2))
        model.add(Dense(num_classes, activation='softmax'))
        #model.summary()

        model.compile(loss='categorical_crossentropy',
                      optimizer=RMSprop(),
                      metrics=['accuracy'])

        return model


    def _learn_model(self, model):

        history = model.fit(x_train, y_train,
                            batch_size=batch_size,
                            epochs=epochs,
                            verbose=1,
                            validation_data=(x_test, y_test))

        score = model.evaluate(x_test, y_test, verbose=0)

        return score[1] #accuracy

# add coordinates to list, check calculated coordinates in cash
    def _cached_call(self, X):

        for ls in self.list:
            print(ls)
            if np.linalg.norm(X - ls[0:-1]) < TOL: # ls[i]=[1, 1, 2] 2 - результат, ls[0:-1] = [1,1]
                print ('I take cash value')
                return ls[-1]

        print ('I invoke new values')

        # model_ = self._create_model(X[0], X[1])
        # accuracy = self._learn_model(model_)
        accuracy = (X[0]-0.5)*(X[1]-0.5)
        self.list.append(np.append(X, accuracy))

        # saving
        #self._save_results()

        return accuracy

# cached_call fuction for multicoordinates
    def _multi_call_protector(self, X):
        print(X)
        if X.ndim == 1:
            return self._cached_call(X) # ???
        else:
            y = []
            for x in X:
                y.append(self._cached_call(x))
            return np.array(y)

# function for saving results of previous calculations -> cached_call
    def save_results(self):
        global SAVE_COUNTER
        SAVE_COUNTER += 1
        np.save('{0}'.format(SAVE_COUNTER), np.array(self.list))

# generate first points for kriging
    def pretrain(self, dim=2, pnts=3):
        sp = samplingplan(dim) # Указываем размерность
        X = sp.optimallhc(pnts) # Генерация точек измерения
        y = []
        for x in X:
            y.append(self._multi_call_protector(x))

        self.k = kriging(X, y, name='simple')
        self.k.train()

# add new point for kriging plot with calculated value
    def infill(self, numIter=1, numInfill=1):
        for i in range(numIter):
            newpoints = self.k.infill(numInfill)
            for point in newpoints:
                self.k.addPoint(point, self._cached_call(point))
        self.k.train()

# draw plot for kriging
    def plot(self):
        self.k.plot()

