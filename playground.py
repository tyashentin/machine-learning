from krigingOptimizer import krigingOptimizer

# using of Optimizer class

opt = krigingOptimizer.classOptimizer()
opt.pretrain()
opt.plot()
opt.save_results()

opt.infill()
opt.plot()
opt.save_results()
